/**
 *  disasm_x86_64
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: Creative Commons CC0
 *          No Rights Reserved
 *          https://creativecommons.org/choose/zero/ 
 *
 * Copyright 2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "table_x86_64.h"

struct _table_x86_64 table_x86_64[] =
{
  { "nop",    0x90000000, 0xff000000, OP_NONE },
  { "ret",    0xc3000000, 0xff000000, OP_NONE },
  { "jz",     0x74000000, 0xff000000, OP_OFFSET_SHORT },
  { "jnz",    0x75000000, 0xff000000, OP_OFFSET_SHORT },
  { "jz",     0x0f840000, 0xffff0000, OP_OFFSET_LONG },
  { "jnz",    0x0f850000, 0xffff0000, OP_OFFSET_LONG },
  { "add",    0x82c00000, 0xfef80000, OP_SHIFT_IMM8 },
  { "add",    0x00000000, 0xfc000000, OP_REG_REG },
  { "add",    0x80000000, 0xfe000000, OP_REG_IMM8 },
  { "and",    0x24000000, 0xfe000000, OP_REGA_IMM8 },
  { "and",    0x80000000, 0xff000000, OP_REG_IMM8 }, // error?
  { "call",   0xff000000, 0xff000000, OP_REG },
  { "mov",    0x88000000, 0xfc000000, OP_REG_REG },
  { "mov",    0xb8000000, 0xfe000000, OP_REGA_IMM32 },
  { "mov",    0xb0000000, 0xfe000000, OP_REGA_IMM8 },
  { "movups", 0x0f100000, 0xfffe0000, OP_REG64_XMM },
  { "or",     0x08000000, 0xfc000000, OP_REG_REG },
  { "shl",    0xc0e00000, 0xfef80000, OP_SHIFT_IMM8 },
  { "shr",    0xc0e80000, 0xfef80000, OP_SHIFT_IMM8 },
  { "sub",    0x28000000, 0xfc000000, OP_REG_REG },
  { "sub",    0x82e80000, 0xfef80000, OP_SHIFT_IMM8 },
  { "test",   0xa8000000, 0xfe000000, OP_REGA_IMM8 },
  { "test",   0xf4000000, 0xfc000000, OP_REG_IMM8 },
  { "test",   0x84000000, 0xfc000000, OP_REG_REG },
  { "xor",    0x30000000, 0xfc000000, OP_REG_REG },
#if 0
  { "add",    0x48000000, 0xfffc0000, OP_REG64_REG },
  { "add",    0x00000000, 0xfc000000, OP_REG32_REG32 },
  { "add",    0x4883c000, 0xfffff800, OP_REG64_IMM8 },
  { "add",    0x83c00000, 0xfff80000, OP_REG32_IMM8 },
  { "mov",    0x48880000, 0xfffc0000, OP_REG64_REG },
  { "mov",    0x88000000, 0xfc000000, OP_REG32_REG32 },
  { "mov",    0x67880000, 0xfffc0000, OP_REG32_REG32_PRE },
  { "mov",    0x66880000, 0xfffc0000, OP_REG64_REG },
  { "shl",    0x48c1e000, 0xfffff800, OP_REG64_IMM8 },
  { "shl",    0xc0e00000, 0xfef80000, OP_REG32_IMM8 },
  { "shr",    0x48c1e800, 0xfffff800, OP_REG64_IMM8 },
  { "shr",    0xc0e80000, 0xfef80000, OP_REG32_IMM8 },
  { "sub",    0x48280000, 0xfffc0000, OP_REG64_REG },
  { "sub",    0x28000000, 0xfc000000, OP_REG32_REG32 },
  { "sub",    0x4883e800, 0xfffff800, OP_REG64_IMM8 },
  { "test",   0x48840000, 0xfffc0000, OP_REG64_REG },
  { "test",   0xa8000000, 0xfe000000, OP_REGA_IMM8 },
  { "test",   0x48a80000, 0xfffe0000, OP_REGA_IMM8 },
  { "test",   0xf6c00000, 0xfff80000, OP_REG32_IMM8 },
  { "test",   0xf7c00000, 0xfff80000, OP_REG32_IMM8_LONG },
  { "test",   0x48f7c000, 0xfffff800, OP_REG64_IMM32 },
#endif
  { NULL, 0, 0, 0 }
};

