/**
 *  disasm_x86_64
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: Creative Commons CC0
 *          No Rights Reserved
 *          https://creativecommons.org/choose/zero/ 
 *
 * Copyright 2017 by Michael Kohn
 *
 */

#ifndef DISASSEMBLER
#define DISASSEMBLER

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

class Disassembler
{
public:
  Disassembler() { }

  Disassembler(FILE *fp) : fp(fp)
  {
    printf("Here\n");
  }

  virtual ~Disassembler() { }

  virtual int Disasm(uint8_t *code, int offset) = 0;

protected:
  FILE *fp;
};

#endif

