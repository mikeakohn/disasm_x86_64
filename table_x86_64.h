/**
 *  disasm_x86_64
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: Creative Commons CC0
 *          No Rights Reserved
 *          https://creativecommons.org/choose/zero/ 
 *
 * Copyright 2017 by Michael Kohn
 *
 */

#ifndef TABLE_X86_64
#define TABLE_X86_64

#include <stdint.h>

enum
{
  OP_NONE,
  OP_OFFSET_SHORT,
  OP_OFFSET_LONG,
  OP_SHIFT_IMM8,
  OP_REG_IMM8,
  OP_REG_REG,
  OP_REG64_XMM,
  OP_REGA_IMM8,
  OP_REGA_IMM32,
  OP_REG,
};

struct _table_x86_64
{
  const char *instr;
  uint32_t opcode;
  uint32_t mask;
  uint8_t type;
};

extern struct _table_x86_64 table_x86_64[];

#endif

