
CFLAGS=-Wall -O3

default:
	g++ -c DisassemblerX86_64.cxx $(CFLAGS)
	g++ -c table_x86_64.cxx $(CFLAGS)
	g++ -o disasm_x86_64 disasm_x86_64.cxx \
	   DisassemblerX86_64.o table_x86_64.o \
	   $(CFLAGS)

test:
	nasm -o out.bin test.asm
	ndisasm -b 64 out.bin

clean:
	@rm -f disasm_x86_64 *.o *.bin
	@echo "Clean!"

