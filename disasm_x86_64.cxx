/**
 *  disasm_x86_64
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: Creative Commons CC0
 *          No Rights Reserved
 *          https://creativecommons.org/choose/zero/ 
 *
 * Copyright 2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "DisassemblerX86_64.h"

int main(int argc, char *argv[])
{
  FILE *fp;
  DisassemblerX86_64 *disasm;
  uint8_t code[4096];
  int len, ptr, n;

  if (argc != 2)
  {
    printf("Usage: %s <filename.bin>\n", argv[0]);
    exit(0);
  }

  fp = fopen(argv[1], "rb");

  if (fp == NULL)
  {
    printf("Could not open file %s\n", argv[1]);
    exit(1);
  }

  len = fread(code, 1, sizeof(code), fp);
  fclose(fp);

  //fp = fopen("out", "wb");
  fp = stdout;

  disasm = new DisassemblerX86_64(fp);

  fprintf(fp, "output\n");

  ptr = 0;

  while(ptr < len)
  {
    n = disasm->Disasm(code, ptr);

    if (n == 0) { break; }

    ptr += n;
  }

  delete disasm;
  //fclose(fp);

  return 0;
}

