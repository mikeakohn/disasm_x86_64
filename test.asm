
bits 64

main:
  jz main
  jne main
  jz far_label
  jne far_label
  ret

  ;add byte [rax], 3
  ;add byte [rax+8], 3
  ;add byte [rax+128], 3

  ;add word [rax], 3
  ;add word [rax+8], 3
  ;add word [rax+128], 3

  ;add dword [rax], 3
  ;add dword [rax+8], 3
  ;add dword [rax+128], 3

  ;add qword [rax], 3
  ;add qword [rax+8], 3
  ;add qword [rax+128], 3

  shl eax, 3
  shl ecx, 3
  shl edx, 3
  shl ebx, 3

  shr eax, 3
  shr ecx, 3
  shr edx, 3
  shr ebx, 3
  shr r9w, 3

  shr rax, 3
  shr rcx, 3
  shr rdx, 3
  shr rbx, 3
  shr r9, 3

  shr ax, 3
  shr cx, 3
  shr dx, 3
  shr bx, 3
  shr r9w, 3

  shr ah, 3
  shr ch, 3
  shr dh, 3
  shr bh, 3

  shr al, 3

  add eax, 3
  add eax, 3

  add rax, 3
  add rcx, 3
  add rdx, 3
  add rbx, 3

  sub rax, 3
  sub rcx, 3
  sub rdx, 3
  sub rbx, 3

  ;add rax, 128
  ;add rcx, 128
  ;add rdx, 128
  ;add rbx, 128

  ;sub rax, 128
  ;sub rcx, 128
  ;sub rdx, 128
  ;sub rbx, 128

  add rax, rbx
  add eax, ebx
  add ax, bx
  add ah, bh

  mov [eax], rbx

  add [rax], rbx
  sub [rax], rbx
  sub [rax], rbx

  sub [rax], ebx
  sub [rax], bx
  sub [rax], bh

  mov [rax], rbx
  mov rbx, [rax]

  mov rax, rbx
  mov rbx, rax
  mov rdi, rax
  mov rsp, rax
  mov rbp, rax

  mov ebx, eax

  mov rax, [rax]
  mov rax, rax
  mov rbx, rax
  mov rcx, rax
  mov rdx, rax

  mov rax, rbx
  mov rbx, rbx
  mov rcx, rbx
  mov rdx, rbx

  mov rax, rsi
  mov rbx, rsi
  mov rcx, rsi
  mov rdx, rsi

  mov rax, [rsi]
  mov rbx, [rsi]
  mov rcx, [rsi]
  mov rdx, [rsi]

  nop
  nop
  ;mov ax, [rsi]
  ;mov bx, [rsi]
  ;mov cx, [rsi]
  ;mov dx, [rsi]

  mov rcx, [rsi+8]
  mov rcx, [rsi-8]
  mov rcx, [rsi+128]
  mov rcx, [rsi-128]

  mov [esi], eax
  mov eax, [esi]

  mov [rsi], eax

  mov eax, [rsi]
  mov ecx, [rsi]
  mov edx, [rsi]
  mov ebx, [rsi]

  mov eax, [rdi]
  mov ecx, [rdi]
  mov edx, [rdi]
  mov ebx, [rdi]

  mov ax, [rdi]
  mov cx, [rdi]
  mov dx, [rdi]
  mov bx, [rsi]
  mov [rdi], bx

  mov [rdi], al
  mov [rdi], cl
  mov [rdi], dl
  mov [rdi], bl

  mov [rdi], ah
  mov [rdi], ch
  mov [rdi], dh
  mov [rdi], bh

  movups xmm0, [rsi]
  movups xmm1, [rsi]
  movups xmm2, [rsi]

  movups [rsi], xmm0
  movups [rsi], xmm1
  movups [rsi], xmm2

  movups xmm0, [rdi]
  movups xmm1, [rdi]
  movups xmm2, [rdi]

  movups [rdi], xmm0
  movups [rdi], xmm1
  movups [rdi], xmm2

  movups [rdi+8], xmm2
  movups [rdi+128], xmm2

  nop
far_label:
  jnz main
  test al,0x20
  test ah,0x20
  test cl,0x20
  test dl,0x20
  test bl,0x20

  test eax,0x20
  test ecx,0x20
  test edx,0x20
  test ebx,0x20

  test rax,0x20
  test rcx,0x20
  test rdx,0x20
  test rbx,0x20

  mov rax,0x7f6c86821850
  mov eax,0x7f6c8682
  mov ax,0x7f6c
  mov al,0x7f
  mov ax,0x8
  mov rax,0x8

  call rax
  call rdx

  ret

