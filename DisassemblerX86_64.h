/**
 *  disasm_x86_64
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: Creative Commons CC0
 *          No Rights Reserved
 *          https://creativecommons.org/choose/zero/ 
 *
 * Copyright 2017 by Michael Kohn
 *
 */

#ifndef DISASSEMBLER_X86_64
#define DISASSEMBLER_X86_64

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "Disassembler.h"

class DisassemblerX86_64 : public Disassembler
{
public:
  DisassemblerX86_64(FILE *fp)
  {
    //Disassembler::Disassembler(fp);
    //Disassembler(fp);

    this->fp = fp;
  }

  virtual ~DisassemblerX86_64() { }

  virtual int Disasm(uint8_t *code, int offset);

private:
  struct _rex
  {
    uint8_t w;
    uint8_t r;
    uint8_t x;
    uint8_t b;
  };

  struct _prefix
  {
    uint8_t operand;
    uint8_t address;
    uint8_t extended;
  };

  int Lookup(uint8_t *code, int offset, char *text);
  int GetRmName(char *name, const char *reg, int mod, const uint8_t *code, int offset);
};

#endif

