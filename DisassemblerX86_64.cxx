/**
 *  disasm_x86_64
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: Creative Commons CC0
 *          No Rights Reserved
 *          https://creativecommons.org/choose/zero/ 
 *
 * Copyright 2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "DisassemblerX86_64.h"
#include "table_x86_64.h"

// http://pnx.tf/files/x86_opcode_structure_and_instruction_overview.png
// http://wiki.osdev.org/X86-64_Instruction_Encoding
// http://www.c-jump.com/CIS77/CPU/x86/X77_0110_scaled_indexed.htm

#define WIDTH 6

#define READ_WORD(a) \
  ((code[offset + (a + 1)] << 8) | \
    code[offset + (a + 0)])

#define READ_DWORD(a) \
  ((code[offset + (a + 3)] << 24) | \
   (code[offset + (a + 2)] << 16) | \
   (code[offset + (a + 1)] << 8) | \
    code[offset + (a + 0)])

#define READ_QWORD(a) \
  (((uint64_t)code[offset + (a + 7)] << 56) | \
   ((uint64_t)code[offset + (a + 6)] << 48) | \
   ((uint64_t)code[offset + (a + 5)] << 40) | \
   ((uint64_t)code[offset + (a + 4)] << 32) | \
   ((uint64_t)code[offset + (a + 3)] << 24) | \
   ((uint64_t)code[offset + (a + 2)] << 16) | \
   ((uint64_t)code[offset + (a + 1)] << 8) | \
    (uint64_t)code[offset + (a + 0)])

#if 0
struct _rex
{
  uint8_t w;
  uint8_t r;
  uint8_t x;
  uint8_t b;
};
#endif

static const char *reg8_name[] =
{
  "al",
  "cl",
  "dl",
  "bl",
  "ah",
  "ch",
  "dh",
  "bh",
  "r8l",
  "r9l",
  "r10l",
  "r11l",
  "r12l",
  "r13l",
  "r14l",
  "r15l",
};

static const char *reg16_name[] =
{
  "ax",
  "cx",
  "dx",
  "bx",
  "sp",
  "bp",
  "si",
  "di",
  "r8w",
  "r9w",
  "r10w",
  "r11w",
  "r12w",
  "r13w",
  "r14w",
  "r15w",
};

static const char *reg32_name[] =
{
  "eax",
  "ecx",
  "edx",
  "ebx",
  "esp",
  "ebp",
  "esi",
  "edi",
  "r8d",
  "r9d",
  "r10d",
  "r11d",
  "r12d",
  "r13d",
  "r14d",
  "r15d",
};

static const char *reg64_name[] =
{
  "rax",
  "rcx",
  "rdx",
  "rbx",
  "rsp",
  "rbp",
  "rsi",
  "rdi",
  "r8",
  "r9",
  "r10",
  "r11",
  "r12",
  "r13",
  "r14",
  "r15",
};

int DisassemblerX86_64::Disasm(uint8_t *code, int offset)
{
  char text[128];
  int len, i;

  len = Lookup(code, offset, text);

  printf("%04x: ", offset);

  if (len == 0)
  {
    fprintf(fp, "%02x           ???\n", code[offset]);
  }
  else
  {
    for (i = 0; i < len; i++)
    {
      if ((i % WIDTH) == 0)
      {
        if (i == WIDTH)
        {
          fprintf(fp, "%s\n", text);
        }

        if (i != 0)
        {
          fprintf(fp, "      ");
        }
      }

      fprintf(fp, "%02x ", code[offset + i]);
    }

    if (i <= WIDTH)
    {
      while(i != WIDTH)
      {
        fprintf(fp, "   ");
        i++;
      }

      fprintf(fp, "%s\n", text);
    }
    else
    {
      fprintf(fp, "\n");
    }
  }

  return len;
}

int DisassemblerX86_64::Lookup(uint8_t *code, int offset, char *text)
{
  struct _rex rex;
  struct _prefix prefix;
  uint32_t opcode;
  const char *rm_name;
  const char *reg_name;
  char operand[32];
  int start = offset;
  int mod, reg, rm, d, l;
  int immediate;
  int temp;
  int n;

  memset(&rex, 0, sizeof(rex));
  memset(&prefix, 0, sizeof(prefix));

  while(1)
  {
    if ((code[offset] & 0xf0) == 0x40)
    {
      rex.w = ((code[offset] & 0x08) == 8) ? 1 : 0;
      rex.r = ((code[offset] & 0x04) == 4) ? 1 : 0;
      rex.x = ((code[offset] & 0x02) == 2) ? 1 : 0;
      rex.b = ((code[offset] & 0x01) == 1) ? 1 : 0;
      offset++;

      continue;
    }

    if (code[offset] == 0x66)
    {
      prefix.operand = 1; 
      offset++;
      continue;
    }

    if (code[offset] == 0x67)
    {
      prefix.address = 1; 
      offset++;
      continue;
    }

#if 0
    if (code[offset] == 0x0f)
    {
      prefix.extended = 1; 
      offset++;
      continue;
    }
#endif

    break;
  }

#if 0
  printf("offset=%d start=%d\n", offset, start);
  printf("rex[w=%d r=%d x=%d b=%d]\n", rex.w, rex.r, rex.x, rex.b);
  printf("prefix[operand=%d address=%d extended=%d]\n", prefix.operand, prefix.address, prefix.extended);
#endif

  opcode = (code[offset + 0] << 24) |
           (code[offset + 1] << 16) |
           (code[offset + 2] << 8) |
            code[offset + 3];

  d = (code[offset + 0]  >> 1) & 0x1;
  l =  code[offset + 0] & 0x1;
  mod =  code[offset + 1] >> 6;
  reg = (code[offset + 1] >> 3) & 0x7;
  rm  =  code[offset + 1] & 0x7;

  if (rex.r == 1) { reg |= 8; }
  if (rex.b == 1) { rm |= 8; }

  n = 0;
  while(table_x86_64[n].instr != NULL)
  {
    if ((opcode & table_x86_64[n].mask) == table_x86_64[n].opcode)
    {
      switch(table_x86_64[n].type)
      {
        case OP_NONE:
        {
          sprintf(text, "%s", table_x86_64[n].instr);
          return (offset - start) + 1;
        }
        case OP_OFFSET_SHORT:
        {
          temp = (int8_t)code[offset + 1];
          sprintf(text, "%s 0x%x (offset=%d)",
              table_x86_64[n].instr, offset + 2 + temp, temp);
          return 2;
        }
        case OP_OFFSET_LONG:
        {
          temp = READ_DWORD(2);
          sprintf(text, "%s 0x%x (offset=%d)",
              table_x86_64[n].instr, offset + 6 + temp, temp);
          return 6;
        }
        case OP_SHIFT_IMM8:
        {
          offset += 2;

          if (rex.w == 1)
          {
            rm_name = reg64_name[rm];
          }
          else if (prefix.operand == 1)
          {
            rm_name = reg16_name[rm];
          }
          else if (l == 0)
          {
            rm_name = reg8_name[rm];
          }
          else
          {
            rm_name = reg32_name[rm];
          }

          offset += GetRmName(operand, rm_name, mod, code, offset);

          immediate = (int8_t)code[offset++];

          sprintf(text, "%s %s, %d",
              table_x86_64[n].instr,
              operand,
              immediate);

           return offset - start;
        }
        case OP_REG_IMM8:
        {
          offset += 2;

          if (rex.w == 1)
          {
            rm_name = reg64_name[rm];
          }
          else if (prefix.operand == 1)
          {
            rm_name = reg16_name[rm];
          }
          else if (l == 0)
          {
            rm_name = reg8_name[rm];
          }
          else
          {
            rm_name = reg32_name[rm];
          }

          offset += GetRmName(operand, rm_name, mod, code, offset);

          if (l == 0)
          {
            immediate = (int8_t)code[offset++];
          }
          else
          {
            immediate = READ_DWORD(0);
            offset += 4;
          }

          sprintf(text, "%s %s, %d",
              table_x86_64[n].instr,
              operand,
              immediate);

           return offset - start;
        }
        case OP_REG_REG:
        {
          offset += 2;

          if (rex.w == 1)
          {
            rm_name = reg64_name[rm];
            reg_name = reg64_name[reg];
          }
          else if (prefix.operand == 1)
          {
            rm_name = reg16_name[rm];
            reg_name = reg16_name[reg];
          }
          else if (l == 0)
          {
            rm_name = reg8_name[rm];
            reg_name = reg8_name[reg];
          }
          else
          {
            rm_name = reg32_name[rm];
            reg_name = reg32_name[reg];
          }

          if (mod != 3)
          {
            if (prefix.address == 0)
            {
              rm_name = reg64_name[rm];
            }
            else
            {
              rm_name = reg32_name[rm];
            }
          }

          // W T F ????  When RSP is used for addressing there
          // always seems to be a 0x24 byte after.  WHY?  I can't
          // find this documentated anywhere.
          if (rm == 4 && mod != 3 && code[offset] == 0x24)
          {
            offset++;
          }

          offset += GetRmName(operand, rm_name, mod, code, offset);

          if (d == 0)
          {
            sprintf(text, "%s %s, %s",
                table_x86_64[n].instr,
                operand,
                reg_name);
          }
          else
          {
            sprintf(text, "%s %s, %s",
                table_x86_64[n].instr,
                reg_name,
                operand);
          }

          return offset - start;
        }
        case OP_REG64_XMM:
        {
          d = code[offset + 1] & 0x1;
          mod =  code[offset + 2] >> 6;
          reg = (code[offset + 2] >> 3) & 0x7;
          rm  =  code[offset + 2] & 0x7;

          offset += 3;

          rm_name = reg64_name[rm];

          offset += GetRmName(operand, rm_name, mod, code, offset);

          if (d == 0)
          {
            sprintf(text, "%s xmm%d, %s", table_x86_64[n].instr, reg, operand);
          }
          else
          {
            sprintf(text, "%s %s, xmm%d", table_x86_64[n].instr, operand, reg);
          }

          return offset - start;
        }
        case OP_REGA_IMM8:
        {
          if (rex.w == 1)
          {
            if (l == 1)
            {
               uint32_t immediate = READ_DWORD(1);
               offset += 5;

               sprintf(text, "%s rax, 0x%x", table_x86_64[n].instr, immediate);
            }
            else
            {
               uint64_t immediate = READ_QWORD(1);
               offset += 9;

               sprintf(text, "%s rax, 0x%lx", table_x86_64[n].instr, immediate);
            }

            return offset - start;
          }

          if (l == 0)
          {
            immediate = (int8_t)code[offset + 1];
            offset += 2;

            sprintf(text, "%s al, %d", table_x86_64[n].instr, immediate);

            return offset - start;
          }
          else
          {
            immediate = READ_DWORD(1);
            offset += 5;

            sprintf(text, "%s eax, %d", table_x86_64[n].instr, immediate);

            return offset - start;
          }
        }
        case OP_REGA_IMM32:
        {
          if (rex.w == 1)
          {
            if (l == 1)
            {
               uint32_t immediate = READ_DWORD(1);
               offset += 5;

               sprintf(text, "%s rax, 0x%x", table_x86_64[n].instr, immediate);
            }
            else
            {
               uint64_t immediate = READ_QWORD(1);
               offset += 9;

               sprintf(text, "%s rax, 0x%lx", table_x86_64[n].instr, immediate);
            }

            return offset - start;
          }

          if (prefix.operand == 1)
          {
            immediate = READ_WORD(1);
            offset += 3;

            sprintf(text, "%s ax, 0x%x", table_x86_64[n].instr, immediate);

            return offset - start;
          }
          else if (l == 0)
          {
            immediate = READ_DWORD(1);
            offset += 5;

            sprintf(text, "%s eax, 0x%x", table_x86_64[n].instr, immediate);

            return offset - start;
          }
          else
          {
            immediate = (int8_t)code[offset + 1];
            offset += 2;

            sprintf(text, "%s al, %d", table_x86_64[n].instr, immediate);

            return offset - start;
          }
        }
        case OP_REG:
        {
          sprintf(text, "%s %s", table_x86_64[n].instr, reg64_name[rm]);
          offset += 2;

          return offset - start;
        }
        default:
          return 0;
      }
    }

    n++;
  }

  return 0;
}

int DisassemblerX86_64::GetRmName(char *name, const char *reg, int mod, const uint8_t *code, int offset)
{
  int n;

  switch(mod)
  {
    case 0:
      sprintf(name, "[%s]", reg);
      return 0;
    case 1:
      n = (int8_t)code[offset];
      sprintf(name, "[%s%s%d]", reg, n >= 0 ? "+":"", n);
      return 1;
    case 2:
      n = READ_DWORD(0);
      sprintf(name, "[%s%s%d]", reg, n >= 0 ? "+":"", n);
      return 4;
    case 3:
      sprintf(name, "%s", reg);
      return 0;
    default:
      sprintf(name, "-%d-", mod);
      break;
  }

  return 0;
}

